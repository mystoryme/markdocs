// Generated using webpack-cli https://github.com/webpack/webpack-cli

import path from "path";
import HtmlWebpackPlugin from "html-webpack-plugin";
import WorkboxWebpackPlugin from "workbox-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import CopyPlugin from "copy-webpack-plugin";
import { globSync } from 'glob';
import { fileURLToPath } from "url";

const __dirname = path.dirname(fileURLToPath(import.meta.url));
const out_dir = path.resolve(__dirname, "dist");
const isProduction = process.env.NODE_ENV == "production";
const stylesHandler = "style-loader";

const mdFiles = globSync('./src/markdown/**/*.md', { nocase: true, posix: true }).sort((a, b) => a.localeCompare(b)).map((value) => ({
  template: value,
  filename: value.replace(/^src\/markdown/i, 'html').replace(/\.md$/i, '.html'),
}));

const mdPlugins = mdFiles.map((value) => {
  return new HtmlWebpackPlugin({
    ...value,
    inject: false,
  })
})

const config = {
  entry: {
    main: "./core/main.jsx",
    page: "./core/page.js",
  },
  output: {
    filename: '[name].bundle.js',
    path: out_dir
  },
  devServer: {
    open: true,
    host: "localhost",
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: "./core/index.html",
      excludeChunks: ['page'],
      templateParameters: {
        initValue: JSON.stringify({
          pageFiles: mdFiles
        })
      }
    }),

    // Add your plugins here
    // Learn more about plugins from https://webpack.js.org/configuration/plugins/
    new HtmlWebpackPlugin({
      template: "./core/page.html",
      filename: 'page.html',
      chunks: ['page'],
      inject: 'body'
    }),
    ...(mdPlugins),
    new CopyPlugin({
      patterns: [
        { from: "./src/assets", to: path.join(out_dir, "assets") },
      ],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/i,
        loader: "babel-loader",
      },
      {
        test: /\.css$/i,
        use: [stylesHandler, "css-loader"],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [stylesHandler, "css-loader", "sass-loader"],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
        type: "asset",
      },

      // Add your rules for custom modules here
      // Learn more about loaders from https://webpack.js.org/loaders/
      {
        test: /\.md$/i,
        use: [
          {
            loader: "html-loader",
          },
          {
            loader: "markdown-loader",
            options: {
              // Pass options to marked
              // See https://marked.js.org/using_advanced#options
              baseUrl: '/src/'
            },
          },
        ],
      },
    ],
  },
};

export default () => {
  if (isProduction) {
    config.mode = "production";

    config.plugins.push(new WorkboxWebpackPlugin.GenerateSW());
  } else {
    config.mode = "development";
  }
  return config;
};
