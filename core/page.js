import './page.scss';

const mainElement = document.getElementById('main')

document.addEventListener('DOMContentLoaded', () => {
    mermaid.initialize();
    const path = new URLSearchParams(location.search).get('p')
    loadPage(path);
})

function loadPage(path) {
    const dir = 'html';
    fetch(dir + path).then((value) => value.text()).then((value) => {
        mainElement.innerHTML = value;
        mainElement.querySelectorAll('pre > code:not(.language-mermaid)').forEach(el => {
            hljs.highlightElement(el)
        });
        mermaid.run({
            nodes: mainElement.querySelectorAll('pre > code.language-mermaid')
        });
    })
}